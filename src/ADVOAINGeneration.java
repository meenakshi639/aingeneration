import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.ancientprogramming.fixedformat4j.*;
import com.ancientprogramming.fixedformat4j.format.*;
import com.ancientprogramming.fixedformat4j.format.impl.*;
import com.ancientprogramming.fixedformat4j.annotation.*;

class Task2 implements Runnable {
	private String[] apiParameters;
	private String lineInputData;

	public Task2(String[] inputParams, String line) {
		apiParameters = inputParams;
		lineInputData = line;

	}

	// Prints task name and sleeps for 1s
	// This Whole process is repeated 5 times

	public void run() {
		JSONObject rsFromCAS = AINGeneration.callCorrectAddress(apiParameters[0], apiParameters[1], apiParameters[2],
				apiParameters[3], apiParameters[4]);
		String ain = AINGeneration.getAINFromCAS(rsFromCAS);
		String stdAddress = AINGeneration.getStdAddressFromCAS(rsFromCAS);
		ADVOAINGeneration.appendAINStdAddress(lineInputData, ain, stdAddress);
	}
}

public class ADVOAINGeneration {
	Map<String, String> propertiesMap = new HashMap<>();
	BufferedReader brInputDataFile;
	static Writer fileWriter;
	String[] fields;
	static int fixedlength = 155;
	Map<String, String> fieldsMap = new HashMap<>();
	String[] apiParameters = new String[5];
	String delims = ",";
	ExecutorService pool;
	private FixedFormatManager manager = new FixedFormatManagerImpl();

	ADVOAINGeneration(Map<String, String> propertiesMap) {
		try {

			String line1 = propertiesMap.get("advo_fields").replaceAll("\\s", "");// WHAT IS GOING TO HAPPEN IF THE
																					// propertiesMap IS WITHOUT DATA
			fields = line1.split(delims);// SAME AS BEFORE--if propertiesMap was not Settled the line1 will be
											// null -- EXCEPTION
			for (int i = 0; i < fields.length; i++) {
				fieldsMap.put(fields[i], "");
			}			
			brInputDataFile = new BufferedReader(new FileReader(propertiesMap.get("advo_input_file_path")));
			fileWriter = new FileWriter(propertiesMap.get("advo_output_file_path"), false);
			pool = Executors.newFixedThreadPool(10);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	boolean generateAppendAINStdAddr() {
		try {

			String lineInputData;

			while ((lineInputData = brInputDataFile.readLine()) != null) {
				// process the line.
				
				getFieldsValues(lineInputData);
				/*
				 * for(int i=0;i<fields.length;i++) {
				 * System.out.print(myMap.get(fields[i])+" "); } System.out.println();
				 */
				apiParameters[0] = fieldsMap.get("streetnumber") + " " + fieldsMap.get("streetname") + " "
						+ fieldsMap.get("streetnamesuffix");
				apiParameters[1] = "";
				apiParameters[2] = fieldsMap.get("zip");
				apiParameters[3] = fieldsMap.get("city") + " " + fieldsMap.get("state");
				apiParameters[4] = "";
				Runnable r1 = new Task2(apiParameters, lineInputData);
				// passes the Task objects to the pool to execute (Step 3)
				pool.execute(r1);
			}
			pool.shutdown();

			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);

			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	void getFieldsValues(String lineInputData) {

		BasicADVORecord record = manager.load(BasicADVORecord.class, lineInputData);
		fieldsMap.put("zip", record.getZip());
		fieldsMap.put("zip4", record.getZip4());
		fieldsMap.put("streetnumber", record.getStreetnumber());
		fieldsMap.put("unitnumber", record.getUnitnumber());
		fieldsMap.put("streetname", record.getStreetname());
		fieldsMap.put("route", record.getRoute());
		fieldsMap.put("season_deliver_ind", record.getSeason_deliver_ind());
		fieldsMap.put("predirection", record.getPredirection());
		fieldsMap.put("streetnamesuffix", record.getStreetnamesuffix());
		fieldsMap.put("address_vacancy_ind_conv", record.getAddress_vacancy_ind_conv());
		fieldsMap.put("delivery_point_usage_code", record.getDelivery_point_usage_code());
		fieldsMap.put("postdirection", record.getPostdirection());
		fieldsMap.put("3_byte_filler", record.getString_3_byte_filler());
		fieldsMap.put("unitdesignator", record.getUnitdesignator());
		fieldsMap.put("countyname", record.getCountyname());
		fieldsMap.put("updatedate", record.getUpdatedate());
		fieldsMap.put("city", record.getCity());
		fieldsMap.put("state", record.getState());
		fieldsMap.put("createdate", record.getCreatedate());

	}

	static void appendAINStdAddress(String lineInputData, String ain, String stdAddress) {
		try {
			String st = "";
			for (int k = 0; k < fixedlength - lineInputData.length(); k++) {
				st = st + " ";
			}

			synchronized (fileWriter) {
				fileWriter.write(lineInputData + st + ain + stdAddress + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
