import java.io.BufferedReader;
//import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.impl.client.HttpClients;
import org.apache.commons.codec.DecoderException;
import org.apache.http.config.Lookup;
import org.apache.http.impl.conn.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.Date;
import java.sql.Timestamp;

public class AINGeneration {
	static Map<String, String> propertiesMap = new HashMap<>();
	// static HttpClient client = new DefaultHttpClient();
	// static MultiThreadedHttpConnectionManager connectionManager =
	// new MultiThreadedHttpConnectionManager();
	// static HttpClient client;

	public static void main(String args[]) {
		try {
			Date date = new Date();
			Timestamp ts = new Timestamp(date.getTime());
			System.out.println("Timestamp is " + ts);
			long startTime = System.nanoTime();
//			client = new HttpClient(connectionManager);
			String delims = "=";
			// Below reads the property file
			BufferedReader brPropsFile = new BufferedReader(new FileReader(args[1]));
			String line;
			String[] keyValue;

			while ((line = brPropsFile.readLine()) != null) {
				if (line.contains(delims)) {
					keyValue = line.split(delims);
					propertiesMap.put(keyValue[0].trim().toLowerCase(), keyValue[1].trim());
				}
			}
			switch (args[0].toLowerCase()) {
			case "highrisk":
				HighRiskAINGeneration obj = new HighRiskAINGeneration(propertiesMap);
				obj.generateAppendAINStdAddr();
				break;

			case "advo":
				ADVOAINGeneration obj2 = new ADVOAINGeneration(propertiesMap);
				obj2.generateAppendAINStdAddr();
				break;
			case "business":
				BisAINGeneration obj3 = new BisAINGeneration(propertiesMap);
				obj3.generateAppendAINStdAddr();
				break;
			/*
			 * default : exit(0);
			 */
			}
			brPropsFile.close();
			long endTime = System.nanoTime();

			long timeElapsedNano = endTime - startTime;
			long timeElapsedMilli = timeElapsedNano / 1000000;
			double timeElapsedSec = timeElapsedMilli * 0.001;
			double timeElapsedMin = timeElapsedSec / 60;

			System.out.println("Elapsed time in seconds is " + timeElapsedSec);
			System.out.println("Elapsed time in minutes is " + timeElapsedMin);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// This is to get the AIN--ADVO HIGH - BUSINESS
	public static JSONObject callCorrectAddress(String line1, String line2, String zip, String cityState, String firm) {
		JSONObject jo = null;
		try {
			String hostname=propertiesMap.get("CAS_HOSTNAME".toLowerCase());
			String port=propertiesMap.get("CAS_PORT".toLowerCase());
			if(hostname == null || port == null) {
				System.out.println("hostname or port is not provided in properties\n");
				System.exit(0);
			}
			
			String webLink = "http://" + propertiesMap.get("CAS_HOSTNAME".toLowerCase()) + ":"
					+ propertiesMap.get("CAS_PORT".toLowerCase()) + "/?command=tiger&" + "szLine1" + "=" + line1 + "&"
					+ "szLine2" + "=" + line2 + "&" + "szZip" + "=" + zip + "&" + "szCityState" + "=" + cityState + "&"
					+ "szfirm";
			webLink = webLink.replaceAll(" ", "%20");
			webLink = webLink.replaceAll("\u00A0", "%20");
			webLink = webLink.replaceAll("\\s", "");
			// System.out.println("webLinkn is "+webLink);
			URL url = new URL(webLink);
			URI uri = url.toURI();

			HttpGet request = new HttpGet(uri);

			// HttpResponse response = null;
			CloseableHttpResponse response = null;
			// HttpClient client = new DefaultHttpClient();
			CloseableHttpClient client = HttpClients.createDefault();
			// GetMethod get = new GetMethod(webLink);
			// client.executeMethod(get);
			response = client.execute(request);
			// client.close();
			// EntityUtils.consume(response.getEntity());

			// String responseBody = get.getResponseBodyAsStream().toString();
			HttpEntity entity = response.getEntity();
			String responseBody = EntityUtils.toString(entity);
			// System.out.println("response is "+responseBody);
			if (entity != null) {
				// InputStream inStream = entity.getContent();
				try {
					// inStream.read();
					// do something useful with the response
				} finally {
					// Closing the input stream will trigger connection release
					// inStream.close();
				}
			}
			response.close();
			client.close();
			Object obj = new JSONParser().parse(responseBody);

			// typecasting obj to JSONObject
			jo = (JSONObject) obj;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jo;
	}

	public static String getAINFromCAS(JSONObject jo) {

		String zip = (String) jo.get("zip");
		if (zip == null)
			zip = "";
		String addon = (String) jo.get("addon");
		if (addon == null)
			addon = "";
		String dpc = (String) jo.get("DPC");
		if (dpc == null)
			dpc = "";
		String checkdigit = (String) jo.get("Checkdigit");
		if (checkdigit == null)
			checkdigit = "";

		// 4578NULL78985
		String ain = zip + addon + dpc + checkdigit;
		return ain;

	}

	public static String getStdAddressFromCAS(JSONObject jo) {

		String strNam = (String) jo.get("strNam");
		if (strNam == null)
			strNam = "";
		String strnum = (String) jo.get("strnum");
		if (strnum == null)
			strnum = "";
		String preDir = (String) jo.get("preDir");
		if (preDir == null)
			preDir = "";
		String strSfx = (String) jo.get("strSfx");
		if (strSfx == null)
			strSfx = "";
		String postDir = (String) jo.get("postDir");
		if (postDir == null)
			postDir = "";
		String secname = (String) jo.get("secname");
		if (secname == null)
			secname = "";
		String secnum = (String) jo.get("secnum");
		if (secnum == null)
			secnum = "";

		String stdAddress = "";
		if (strNam.equalsIgnoreCase("PO BOX")) {
			stdAddress = strNam + " " + strnum;
		}
		// If the strNam is equal �PO BOX� the standardized will be:
		else {
			stdAddress = strnum + " " + preDir + " " + strNam;
		}
		
			stdAddress += strSfx + " " + postDir + " " + secname + " " + secnum;
		
		return stdAddress;

	}

}
