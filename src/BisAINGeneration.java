import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;

/**
 * @author C56601A
 * Java Class for BisFIleGeneration which will be having AIN Number and Standardized address in 
 * output BIS File this class is dependent on execution of AINGeneration class
 *
 */


public class BisAINGeneration {

	Map<String, String> propertiesMap = new HashMap<>();
	BufferedReader brInputDataFile;
	static Writer fileWriter;
	String[] fields;
	static int countOfFields;
	Map<String, String> fieldsMap = new HashMap<>();
	String[] apiParameters = new String[5];
	String delims = ",";
	
	/**
	 * Constructor call of BisFileGeneration
	 * @param propertiesMap
	 */
	
    BisAINGeneration(Map<String, String> propertiesMap) {
		try {

			String line1 = propertiesMap.get("bis_fields").replaceAll("\\s", "");// WHAT IS GOING TO HAPPEN IF THE
																						// propertiesMap IS WITHOUT DATA
			fields = line1.split(delims);// SAME AS BEFORE--if propertiesMap was not Settled the line1 will be
											// null -- EXCEPTION
			for (int i = 0; i < fields.length; i++) {
				fieldsMap.put(fields[i].toLowerCase(), "");
			}
			countOfFields = fields.length;
			brInputDataFile = new BufferedReader(new FileReader(propertiesMap.get("bis_input_file_path")));
			fileWriter = new FileWriter(propertiesMap.get("bis_output_file_path"), false);
			  //pool = Executors.newFixedThreadPool(10);   
	         
		        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    
    /**
     *  Method to generate AIN Number and Standardized address by reading parameter values from inputDataFile
     *  and append the generated output file with AIN number and Standardized address
     * @return boolean
     */
	boolean generateAppendAINStdAddr() {
		try {

			String delims = "\\|";

			// file
			String lineInputData;

			while ((lineInputData = brInputDataFile.readLine()) != null) {
				// process the line.
				// tokensInputLine contains tokens from input data line
				String[] tokensInputLine = lineInputData.split(delims);
				int tokenCount = tokensInputLine.length;
				for (int j = 0; j < tokenCount; j++) {
					fieldsMap.put(fields[j].toLowerCase(), tokensInputLine[j]);
				}
				/*
				 * for(int i=0;i<fields.length;i++) {
				 * System.out.print(myMap.get(fields[i])+" "); } System.out.println();
				 */
				apiParameters[0] = fieldsMap.get("address");
				apiParameters[1] = "";
				apiParameters[2] = fieldsMap.get("zip");
				apiParameters[3] = fieldsMap.get("city") + " " + fieldsMap.get("state");
				apiParameters[4] = "";
				
				
				
					JSONObject	rsFromCAS = AINGeneration.callCorrectAddress(apiParameters[0], apiParameters[1], apiParameters[2],
						                apiParameters[3], apiParameters[4]);
						        String ain = AINGeneration.getAINFromCAS(rsFromCAS);
						        String stdAddress = AINGeneration.getStdAddressFromCAS(rsFromCAS);
						        BisAINGeneration.appendAINStdAddress(lineInputData, tokenCount, ain, stdAddress);
				
		
				//Runnable r1 = new Task(apiParameters, lineInputData, tokenCount);
				 // passes the Task objects to the pool to execute (Step 3) 
		        //pool.execute(r1);
			}
			//pool.shutdown();
			
			//pool.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
			
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	 /**
	  * Method to write generated AIN Number and Standardized address into output file
	  * 
	  * @param lineInputData
	  * @param tokenCount
	  * @param ain
	  * @param stdAddress
	  */
	static void appendAINStdAddress(String lineInputData, int tokenCount, String ain, String stdAddress) {
		try {
			String st = "";
			for (int k = 0; k < countOfFields - tokenCount; k++) {
				st = st + "|";
			}
	 
			fileWriter.write(lineInputData + st + "|" + ain + "|" + stdAddress + "\n");
			
			} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


