import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import java.util.concurrent.ExecutorService; 
import java.util.concurrent.Executors; 
import java.util.concurrent.TimeUnit;
  
class Task implements Runnable {
	//private String[] apiParameters;
	String inputParams1;
	String inputParams2;
	String inputParams3;
	String inputParams4;
	String inputParams5;
	private String lineInputData;
	int tokenCount;

	public Task(String inputParams1,String inputParams2,String inputParams3,String inputParams4,String inputParams5, String line, int tokenCount) {
		//apiParameters = inputParams;
		this.inputParams1=inputParams1;
		this.inputParams2=inputParams2;
		this.inputParams3=inputParams3;
		this.inputParams4=inputParams4;
		this.inputParams5=inputParams5;
		lineInputData = line;
		this.tokenCount = tokenCount;
		 
		 }	

	public void run() {
		JSONObject rsFromCAS = AINGeneration.callCorrectAddress(inputParams1, inputParams2, inputParams3,
				inputParams4, inputParams5);
		String ain = AINGeneration.getAINFromCAS(rsFromCAS);
		String stdAddress = AINGeneration.getStdAddressFromCAS(rsFromCAS);
		HighRiskAINGeneration.appendAINStdAddress(lineInputData, tokenCount, ain, stdAddress);
	}
}

public class HighRiskAINGeneration {
	Map<String, String> propertiesMap = new HashMap<>();
	BufferedReader brInputDataFile;
	static Writer fileWriter;
	String[] fields;
	static int countOfFields;
	Map<String, String> fieldsMap = new HashMap<>();
	String[] apiParameters = new String[5];
	String delims = ",";
	ExecutorService pool;
	HighRiskAINGeneration(Map<String, String> propertiesMap) {
		try {

			String line = propertiesMap.get("highrisk_fields");
			if(line == null) {
				System.out.println("key highrisk_fields in properties file doesnt exist");
				System.exit(0);
			}
			String line1=line.replaceAll("\\s", "");
			fields = line1.split(delims);
			for (int i = 0; i < fields.length; i++) {
				fieldsMap.put(fields[i].toLowerCase(), "");
			}
			countOfFields = fields.length;
			brInputDataFile = new BufferedReader(new FileReader(propertiesMap.get("highrisk_input_file_path")));
			if(brInputDataFile == null) {
				System.out.println("key highrisk_input_file_path in properties file doesnt exist or referenced file doesnt exist");
				System.exit(0);
			}
			//output file where data has to be written
			fileWriter = new FileWriter(propertiesMap.get("highrisk_output_file_path"), false);
			if(fileWriter == null) {
				System.out.println("key highrisk_output_file_path in properties file doesnt exist or no permission to write here");
				System.exit(0);
			}
			
			  pool = Executors.newFixedThreadPool(10);   
	         
		        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	boolean generateAppendAINStdAddr() {
		try {

			String delims = "\\|";

			String lineInputData;

			while ((lineInputData = brInputDataFile.readLine()) != null) {
				// process the line.
				// tokensInputLine contains tokens from input data line
				String[] tokensInputLine = lineInputData.split(delims);
				int tokenCount = tokensInputLine.length;
				for (int j = 0; j < tokenCount; j++) {
					//fieldsMap contains the mapping from field names to values
					fieldsMap.put(fields[j], tokensInputLine[j]);
				}
				/*
				 * for(int i=0;i<fields.length;i++) {
				 * System.out.print(myMap.get(fields[i])+" "); } System.out.println();
				 */
				apiParameters[0] = fieldsMap.get("address");
				apiParameters[1] = "";
				apiParameters[2] = fieldsMap.get("zip");
				apiParameters[3] = fieldsMap.get("city") + " " + fieldsMap.get("state");
				apiParameters[4] = "";
				Runnable r1 = new Task(apiParameters[0], apiParameters[1],apiParameters[2],apiParameters[3],apiParameters[4],lineInputData, tokenCount);
				 // passes the Task objects to the pool to execute (Step 3) 
		        pool.execute(r1);
			}
			pool.shutdown();
			
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
			
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	static void appendAINStdAddress(String lineInputData, int tokenCount, String ain, String stdAddress) {
		try {
			String st = "";
			for (int k = 0; k < countOfFields - tokenCount; k++) {
				st = st + "|";
			}
			synchronized(fileWriter) {
			fileWriter.write(lineInputData + st + "|" + ain + "|" + stdAddress + "\n");
			}
			} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
