import com.ancientprogramming.fixedformat4j.*;
import com.ancientprogramming.fixedformat4j.format.*;
import com.ancientprogramming.fixedformat4j.format.impl.*;
import com.ancientprogramming.fixedformat4j.annotation.*;
import java.util.Date;
//import org.apache.commons.lang3.time.*;

@Record
public class BasicADVORecord {

	private String zip;
	private String zip4;
	private String streetnumber;
	private String unitnumber;
	private String streetname;
	private String route;
	private String season_deliver_ind;
	private String predirection;
	private String streetnamesuffix;
	private String address_vacancy_ind_conv;
	private String delivery_point_usage_code;
	private String postdirection;
	private String string_3_byte_filler;
	private String unitdesignator;
	private String countyname;
	private String updatedate;
	private String city;
	private String state;
	private String createdate;

	@Field(offset = 1, length = 5)
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Field(offset = 6, length = 4)
	public String getZip4() {
		return zip4;
	}

	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}

	@Field(offset = 10, length = 10)
	public String getStreetnumber() {
		return streetnumber;
	}

	public void setStreetnumber(String streetnumber) {
		this.streetnumber = streetnumber;
	}

	@Field(offset = 20, length = 8)
	public String getUnitnumber() {
		return unitnumber;
	}

	public void setUnitnumber(String unitnumber) {
		this.unitnumber = unitnumber;
	}

	@Field(offset = 28, length = 28)
	public String getStreetname() {
		return streetname;
	}

	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}

	@Field(offset = 56, length = 4)
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	@Field(offset = 60, length = 1)
	public String getSeason_deliver_ind() {
		return season_deliver_ind;
	}

	public void setSeason_deliver_ind(String season_deliver_ind) {
		this.season_deliver_ind = season_deliver_ind;
	}

	@Field(offset = 61, length = 2)
	public String getPredirection() {
		return predirection;
	}

	public void setPredirection(String predirection) {
		this.predirection = predirection;
	}

	@Field(offset = 63, length = 4)
	public String getStreetnamesuffix() {
		return streetnamesuffix;
	}

	public void setStreetnamesuffix(String streetnamesuffix) {
		this.streetnamesuffix = streetnamesuffix;
	}

	@Field(offset = 67, length = 1)
	public String getAddress_vacancy_ind_conv() {
		return address_vacancy_ind_conv;
	}

	public void setAddress_vacancy_ind_conv(String address_vacancy_ind_conv) {
		this.address_vacancy_ind_conv = address_vacancy_ind_conv;
	}

	@Field(offset = 68, length = 1)
	public String getDelivery_point_usage_code() {
		return delivery_point_usage_code;
	}

	public void setDelivery_point_usage_code(String delivery_point_usage_code) {
		this.delivery_point_usage_code = delivery_point_usage_code;
	}

	@Field(offset = 69, length = 2)
	public String getPostdirection() {
		return postdirection;
	}

	public void setPostdirection(String postdirection) {
		this.postdirection = postdirection;
	}

	@Field(offset = 71, length = 3)
	public String getString_3_byte_filler() {
		return string_3_byte_filler;
	}

	public void setString_3_byte_filler(String string_3_byte_filler) {
		this.string_3_byte_filler = string_3_byte_filler;
	}

	@Field(offset = 74, length = 4)
	public String getUnitdesignator() {
		return unitdesignator;
	}

	public void setUnitdesignator(String unitdesignator) {
		this.unitdesignator = unitdesignator;
	}

	@Field(offset = 78, length = 28)
	public String getCountyname() {
		return countyname;
	}

	public void setCountyname(String countyname) {
		this.countyname = countyname;
	}

	@Field(offset = 106, length = 10)
	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	@Field(offset = 116, length = 28)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Field(offset = 144, length = 2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Field(offset = 146, length = 10)
	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

}
